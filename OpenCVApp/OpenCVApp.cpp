#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <stdio.h>

using namespace cv;
using namespace std;

#include <cstdlib>
#include <opencv2\opencv.hpp>
//#include <opencv2\imgcodecs.hpp>
//#include <opencv2\highgui\highgui.hpp>
//#include <vector>
//#include <math.h>


const unsigned NUM_CARDS = 4;
const unsigned NUM_DECK_CARDS = 52;
const int CARD_SIZE_X = 125;
const int CARD_SIZE_Y = 175;

#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"

//MATCHING
/// Global Variables
Mat img; Mat templ; Mat result;
const char* image_window = "Source Image";
const char* result_window = "Result window";

int match_method;
int max_Trackbar = 5;

/// Function Headers
void MatchingMethod(int, void*);
//END MATCHING


bool compareContourAreas(std::vector<cv::Point> contour1, std::vector<cv::Point> contour2) {
	double j = fabs(contourArea(cv::Mat(contour1)));
	double i = fabs(contourArea(cv::Mat(contour2)));
	return (i < j);
}

void rectify(Point2f points[4]) {
	float sum[4];
	float diff[4];
	for (unsigned i = 0; i<4; i++) {
		sum[i] = points[i].x + points[i].y;//std::cout<<"sum["<<i<<"] = "<<sum[i]<<std::endl;
		diff[i] = points[i].x - points[i].y;//std::cout<<"diff["<<i<<"] = "<<diff[i]<<std::endl;
	}
	float smallest_sum, smallest_diff, biggest_sum, biggest_diff;
	smallest_sum = smallest_diff = 10000;
	biggest_sum = biggest_diff = -10000;
	Point2f smallest_sum_p, smallest_diff_p, biggest_sum_p, biggest_diff_p;
	for (unsigned i = 0; i<4; i++) {
		if (sum[i]<smallest_sum) { smallest_sum = sum[i]; smallest_sum_p = points[i]; }
		if (sum[i]>biggest_sum) { biggest_sum = sum[i]; biggest_sum_p = points[i]; }
		if (diff[i]<smallest_diff) { smallest_diff = diff[i]; smallest_diff_p = points[i]; }
		if (diff[i]>biggest_diff) { biggest_diff = diff[i]; biggest_diff_p = points[i]; }
	}
	//for(unsigned i=0;i<4;i++)std::cout<<"points["<<i<<"] = "<<points[i]<<" sum:"<<sum[i]<<" diff:"<<diff[i]<<std::endl;
	points[0] = smallest_sum_p;
	points[3] = smallest_diff_p;
	points[2] = biggest_sum_p;
	points[1] = biggest_diff_p;
	//for(unsigned i=0;i<4;i++)std::cout<<"points["<<i<<"] = "<<points[i]<<std::endl;
}

void rectify2(Point2f points[4]) {
	Point2f pA;
	double best_dis = -1, this_dis;
	unsigned index = 0;
	//distancias p->p2
	for (unsigned i = 0; i<3; i++) {
		this_dis = norm(points[i] - points[i + 1]);
		if (this_dis>best_dis) {
			best_dis = this_dis;
			index = i;
			pA = points[i];
		}
	}
	for (unsigned i = 0; i<4; i++)std::cout << "points[" << i << "] = " << points[i] << std::endl;
	if (index == 0) {
		points[0] = points[1];
		points[1] = points[2];
		points[2] = points[3];
		points[3] = pA;
	}
	else if (index == 2) {
		points[0] = points[1];
		points[1] = points[2];
		points[2] = points[3];
		points[3] = pA;
	}
	for (unsigned i = 0; i<4; i++)std::cout << "points[" << i << "] = " << points[i] << std::endl;
}

void rectify3(Point2f points[4]) {
	//fix horizontal card
	if (norm(points[0] - points[1]) > norm(points[1] - points[2])) {
		Point2f firstP = points[0];
		points[0] = points[1];
		points[1] = points[2];
		points[2] = points[3];
		points[3] = firstP;
	}
	//mirror Y axis
	Point2f temp = points[0];
	points[0] = points[1];
	points[1] = temp;
	temp = points[2];
	points[2] = points[3];
	points[3] = temp;
}

void toPoints(std::vector<Point> approx, Point2f rect_points[4]) {
	for (unsigned i = 0; i<4; i++)
		rect_points[i] = Point2f(approx[i]);
	rectify3(rect_points);
}

void get_cards(Mat img, Mat warps[], unsigned num_cards) {

	Mat gray, blur, thresh;
	std::vector<std::vector<Point>> contours, scontours;
	std::vector<Vec4i> hierarchy;

	// grayscale
	cvtColor(img, gray, CV_BGR2GRAY);

	// blur ?
	GaussianBlur(gray, blur, Size(1, 1), 0, 0, 1000);

	//threshold
	threshold(blur, thresh, 120, 255, THRESH_BINARY);

	//contours
	findContours(thresh, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

	//order by size (cards will be first)
	std::sort(contours.begin(), contours.end(), compareContourAreas);


	//find rectangles
	std::vector<Point> card, approx, r;
	double peri;
	RotatedRect rect;
	Mat transform, warp;
	Point2f h[4] =
		{ Point2f(0,0),Point2f(CARD_SIZE_X - 1,0),
			Point2f(CARD_SIZE_X - 1,CARD_SIZE_Y - 1), Point2f(0,CARD_SIZE_Y - 1) };
	Point2f rect_points[4];

	for (unsigned i = 0; i<num_cards; i++) {
		card = contours[i];
		peri = arcLength(card, true);
		approxPolyDP(card, approx, 0.05*peri, true);//std::cout<<"aprox:\n"<<approx<<std::endl;
													//rect = minAreaRect(approx);
													//rect.points( rect_points);
		toPoints(approx, rect_points);//std::cout<<"rect_points:\n";for(unsigned i=0; i<4; i++) std::cout<<rect_points[i]<<std::endl;
		transform = getPerspectiveTransform(rect_points, h);
		warpPerspective(img, warp, transform, Size(CARD_SIZE_X, CARD_SIZE_Y));
		warps[i] = warp.clone();
	}
}

struct card {
	std::string suit;
	std::string value;
	Mat view;
};

//TODO
//Identify SUIT and VALUE
void load_deck(Mat img, card cards[]) {

	std::string suits[4] = { "H","D","C","S" };
	std::string value[13] = { "A","2","3","4","5","6","7","8","9","10","J","Q","K" };
	Mat gray, blur, thresh;
	//Stores images of cards to recognize
	Mat warps[NUM_DECK_CARDS];
	//image with all cards
	get_cards(img, warps, NUM_DECK_CARDS);
	//                          0    1    2    3    4    5    6    7    8    9    10   11   12   13  14   15    16   17    18   19   20   21   22   23   24  25   26   27    28   29    30   31   32   33   34   35   36   37   38   39    40   41   42   43  44   45   46   47    48  49    50   51    52
	std::string vals[52] = { "A", "2", "A", "3", "A", "2", "4", "A", "2", "3", "5", "9", "6", "2", "4", "10", "7", "J", "3", "Q", "5", "K", "9", "3", "4", "8", "6", "10", "7", "J", "8", "4", "5", "K", "9", "Q", "6", "10", "7", "5", "8", "9", "J", "K", "6", "Q", "10", "7", "J", "Q", "K", "8" };
	std::string nai[52] = { "C", "C", "O", "C", "P", "O", "C", "E", "P", "O", "C", "C", "C", "E", "O",  "C", "C", "C", "P", "C", "O", "C", "O", "E", "P", "C", "O",  "O", "O", "O", "O", "E", "P", "O", "P", "O", "P",  "P", "P", "E", "P", "E", "P", "P", "E", "P",  "E", "E", "E", "E", "E", "E" };

	for (unsigned i = 0; i<NUM_DECK_CARDS; i++) {
		cards[i].suit = vals[i];//std::to_string(i);
		cards[i].value = nai[i];//std::to_string(i);
		cards[i].view = warps[i];
	}
}

void ORBMatching() {
	vector<String> typeDesc;
	vector<String> typeAlgoMatch;
	vector<String> fileName;
	
	// This descriptor are going to be detect and compute
	typeDesc.push_back("AKAZE-DESCRIPTOR_KAZE_UPRIGHT");    // see http://docs.opencv.org/trunk/d8/d30/classcv_1_1AKAZE.html
	typeDesc.push_back("AKAZE");    // see http://docs.opencv.org/trunk/d8/d30/classcv_1_1AKAZE.html
	typeDesc.push_back("ORB");      // see http://docs.opencv.org/trunk/de/dbf/classcv_1_1BRISK.html
	typeDesc.push_back("BRISK");    // see http://docs.opencv.org/trunk/db/d95/classcv_1_1ORB.html
									// This algorithm would be used to match descriptors see http://docs.opencv.org/trunk/db/d39/classcv_1_1DescriptorMatcher.html#ab5dc5036569ecc8d47565007fa518257
	typeAlgoMatch.push_back("BruteForce");
	typeAlgoMatch.push_back("BruteForce-L1");
	typeAlgoMatch.push_back("BruteForce-Hamming");
	typeAlgoMatch.push_back("BruteForce-Hamming(2)");
	
	fileName.push_back("9_of_spades.jpg");
	fileName.push_back("hand.jpg");

	Mat img1 = imread(fileName[0], IMREAD_GRAYSCALE);
	Mat img2 = imread(fileName[1], IMREAD_GRAYSCALE);
	if (img1.rows*img1.cols <= 0)
	{
		cout << "Image " << fileName[0] << " is empty or cannot be found\n";
		return;
	}
	if (img2.rows*img2.cols <= 0)
	{
		cout << "Image " << fileName[1] << " is empty or cannot be found\n";
		return;
	}

	vector<double> desMethCmp;
	Ptr<Feature2D> b;

	// Descriptor loop
	vector<String>::iterator itDesc;
	for (itDesc = typeDesc.begin(); itDesc != typeDesc.end(); itDesc++)
	{
		Ptr<DescriptorMatcher> descriptorMatcher;
		// Match between img1 and img2
		vector<DMatch> matches;
		// keypoint  for img1 and img2
		vector<KeyPoint> keyImg1, keyImg2;
		// Descriptor for img1 and img2
		Mat descImg1, descImg2;
		vector<String>::iterator itMatcher = typeAlgoMatch.end();
		if (*itDesc == "AKAZE-DESCRIPTOR_KAZE_UPRIGHT") {
			b = AKAZE::create(AKAZE::DESCRIPTOR_KAZE_UPRIGHT);
		}
		if (*itDesc == "AKAZE") {
			b = AKAZE::create();
		}
		if (*itDesc == "ORB") {
			b = ORB::create();
		}
		else if (*itDesc == "BRISK") {
			b = BRISK::create();
		}
		try
		{
			//double alpha = 3.0; /**< Simple contrast control */
			//int beta = 0;  /**< Simple brightness control */
			//Mat imgH = descImg1;
			//imgH.convertTo(imgH, -1, alpha, beta);
			// We can detect keypoint with detect method
			b->detect(img1, keyImg1, Mat());
			// and compute their descriptors with method  compute
			b->compute(img1, keyImg1, descImg1);
			// or detect and compute descriptors in one step
			b->detectAndCompute(img2, Mat(), keyImg2, descImg2, false);
			// Match method loop
			for (itMatcher = typeAlgoMatch.begin(); itMatcher != typeAlgoMatch.end(); itMatcher++) {
				descriptorMatcher = DescriptorMatcher::create(*itMatcher);
				if ((*itMatcher == "BruteForce-Hamming" || *itMatcher == "BruteForce-Hamming(2)") && (b->descriptorType() == CV_32F || b->defaultNorm() <= NORM_L2SQR))
				{
					cout << "**************************************************************************\n";
					cout << "It's strange. You should use Hamming distance only for a binary descriptor\n";
					cout << "**************************************************************************\n";
				}
				if ((*itMatcher == "BruteForce" || *itMatcher == "BruteForce-L1") && (b->defaultNorm() >= NORM_HAMMING))
				{
					cout << "**************************************************************************\n";
					cout << "It's strange. You shouldn't use L1 or L2 distance for a binary descriptor\n";
					cout << "**************************************************************************\n";
				}
				try
				{
					
					descriptorMatcher->match(descImg1, descImg2, matches, Mat());
					// Keep best matches only to have a nice drawing.
					// We sort distance between descriptor matches
					Mat index;
					int nbMatch = int(matches.size());
					Mat tab(nbMatch, 1, CV_32F);
					for (int i = 0; i<nbMatch; i++)
					{
						tab.at<float>(i, 0) = matches[i].distance;
					}
					sortIdx(tab, index, SORT_EVERY_COLUMN + SORT_ASCENDING);
					vector<DMatch> bestMatches;
					for (int i = 0; i<30; i++)
					{
						bestMatches.push_back(matches[index.at<int>(i, 0)]);
					}
					Mat result;
					drawMatches(img1, keyImg1, img2, keyImg2, bestMatches, result);
					namedWindow(*itDesc + ": " + *itMatcher, WINDOW_AUTOSIZE);
					imshow(*itDesc + ": " + *itMatcher, result);
					// Saved result could be wrong due to bug 4308
					FileStorage fs(*itDesc + "_" + *itMatcher + ".yml", FileStorage::WRITE);
					fs << "Matches" << matches;
					vector<DMatch>::iterator it;
					cout << "**********Match results**********\n";
					cout << "Index \tIndex \tdistance\n";
					cout << "in img1\tin img2\n";
					// Use to compute distance between keyPoint matches and to evaluate match algorithm
					double cumSumDist2 = 0;
					for (it = bestMatches.begin(); it != bestMatches.end(); it++)
					{
						cout << it->queryIdx << "\t" << it->trainIdx << "\t" << it->distance << "\n";
						Point2d p = keyImg1[it->queryIdx].pt - keyImg2[it->trainIdx].pt;
						cumSumDist2 = p.x*p.x + p.y*p.y;
					}
					desMethCmp.push_back(cumSumDist2);
					waitKey();
				}
				catch (Exception& e)
				{
					cout << e.msg << endl;
					cout << "Cumulative distance cannot be computed." << endl;
					desMethCmp.push_back(-1);
				}
			}
		}
		catch (Exception& e)
		{
			cout << "Feature : " << *itDesc << "\n";
			if (itMatcher != typeAlgoMatch.end())
			{
				cout << "Matcher : " << *itMatcher << "\n";
			}
			cout << e.msg << endl;
		}
	}
	int i = 0;
	cout << "Cumulative distance between keypoint match for different algorithm and feature detector \n\t";
	cout << "We cannot say which is the best but we can say results are differents! \n\t";
	for (vector<String>::iterator itMatcher = typeAlgoMatch.begin(); itMatcher != typeAlgoMatch.end(); itMatcher++)
	{
		cout << *itMatcher << "\t";
	}
	cout << "\n";
	for (itDesc = typeDesc.begin(); itDesc != typeDesc.end(); itDesc++)
	{
		cout << *itDesc << "\t";
		for (vector<String>::iterator itMatcher = typeAlgoMatch.begin(); itMatcher != typeAlgoMatch.end(); itMatcher++, i++)
		{
			cout << desMethCmp[i] << "\t";
		}
		cout << "\n";
	}
	return;
}

void MatchingMethod(int, void*)
{
	/// Source image to display
	Mat img_display;
	img.copyTo(img_display);

	/// Create the result matrix
	int result_cols = img.cols - templ.cols + 1;
	int result_rows = img.rows - templ.rows + 1;

	result.create(result_rows, result_cols, CV_32FC1);


	/// Do the Matching and Normalize
	matchTemplate(img, templ, result, match_method);
	normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

	/// Localizing the best match with minMaxLoc
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point matchLoc;

	minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());


	/// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
	if (match_method == TM_SQDIFF || match_method == TM_SQDIFF_NORMED)
	{
		matchLoc = minLoc;
	}
	else
	{
		matchLoc = maxLoc;
	}

	/// Show me what you got
	rectangle(img_display, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), Scalar::all(0), 2, 8, 0);
	rectangle(result, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), Scalar::all(0), 2, 8, 0);

	imshow(image_window, img_display);
	imshow(result_window, result);

	return;
}

/*void detectOurHand()
{
	//find wich cards u have
	Mat diff[NUM_CARDS][NUM_DECK_CARDS];
	for(unsigned i=0; i<NUM_CARDS; i++) {
	for(unsigned j=0; j<NUM_DECK_CARDS; j++) {
	Mat incard,indeckcard,diffcard;
	//prepare
	cvtColor(cards[i], incard, CV_BGR2GRAY);
	GaussianBlur(incard,incard,Size(7,7),0,0);

	cvtColor(deck[j].view, indeckcard, CV_BGR2GRAY);
	GaussianBlur(indeckcard,indeckcard,Size(1,1),0,0);

	//calc diff
	absdiff( incard, indeckcard, diffcard);

	//threshold
	threshold(diffcard,diffcard,30,255,THRESH_BINARY);

	//show
	imshow("card", incard);
	imshow("deckcard", indeckcard);
	imshow("diff", diffcard);
	waitKey(0);
	}
	}
	
	img1 = cv2.GaussianBlur(img1,(5,5),5)
	img2 = cv2.GaussianBlur(img2,(5,5),5)
	diff = cv2.absdiff(img1,img2)
	diff = cv2.GaussianBlur(diff,(5,5),5)

	//get winner according to rules


	//print over the cards the winner



	//window
	namedWindow( "OpenCV", WINDOW_AUTOSIZE );

	// Draw canny
	imshow( "OpenCV", canny );

	// Draw contours
	Mat drawing = Mat::zeros( canny.size(), CV_8UC3 );
	RNG rng(12345);
	for( int i = 0; i< contours.size(); i++ ){
	Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
	drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
	}
	imshow( "OpenCV", drawing);
	

	/// Draw contours + rotated rects + ellipses
	RNG rng(12345);
	Mat drawing = Mat::zeros( thresh.size(), CV_8UC3 );
	for( size_t i = 0; i< contours.size(); i++ ){
	Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
	// contour
	drawContours( drawing, contours, (int)i, color, 1, 8, std::vector<Vec4i>(), 0, Point() );
	}
	imshow( "OpenCV", drawing );
}*/


void main() {

	ORBMatching();
	/*
	/// Load image and template
	img = imread("hand.jpg");
	templ = imread("9_of_spades.jpg");

	/// Create windows
	namedWindow(image_window, WINDOW_AUTOSIZE);
	namedWindow(result_window, WINDOW_AUTOSIZE);

	/// Create Trackbar
	const char* trackbar_label = "Method: \n 0: SQDIFF \n 1: SQDIFF NORMED \n 2: TM CCORR \n 3: TM CCORR NORMED \n 4: TM COEFF \n 5: TM COEFF NORMED";
	createTrackbar(trackbar_label, image_window, &match_method, max_Trackbar, MatchingMethod);

	MatchingMethod(0, 0);
	*/

	/*Mat img1 = imread("hand.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	Mat img2 = imread("9_of_spades.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	if (img1.empty() || img2.empty())
	{
		printf("Can't read one of the images\n");
	}

	// detecting keypoints
	SurfFeatureDetector detector(400);
	vector<KeyPoint> keypoints1, keypoints2;
	detector.detect(img1, keypoints1);
	detector.detect(img2, keypoints2);

	// computing descriptors
	SurfFeatureDetector extractor;
	Mat descriptors1, descriptors2;
	extractor.compute(img1, keypoints1, descriptors1);
	extractor.compute(img2, keypoints2, descriptors2);

	// matching descriptors
	BFMatcher matcher(NORM_L2);
	vector<DMatch> matches;
	matcher.match(descriptors1, descriptors2, matches);

	// drawing the results
	namedWindow("matches", 1);
	Mat img_matches;
	drawMatches(img1, keypoints1, img2, keypoints2, matches, img_matches);
	imshow("matches", img_matches);*/


	

	/*Mat img, deck_img;

	// load images
	img = imread("hand.jpg");
	deck_img = imread("deck.jpg");

	//get cards in input image
	Mat cards[NUM_CARDS] = {};
	get_cards(img, cards, NUM_CARDS);

	//load deck
	card deck[NUM_DECK_CARDS] = {};
	load_deck(deck_img, deck);


	

	//show image
	imshow("inputImage", img);
	//show deck
	imshow("deck", deck_img);

	

	//Draw cards
	for (unsigned i = 0; i<NUM_CARDS; i++) {
		std::string windowName = "card " + std::to_string(i);
		std::cout << "printing card number " << i << ": " << windowName << std::endl;
		imshow(windowName, cards[i]);
	}

	//Draw deck
	for (unsigned i = 0; i<NUM_DECK_CARDS; i++) {
		std::string windowName = deck[i].suit + " " + deck[i].value;
		std::cout << "printing deck card number " << i << ": " << windowName << std::endl;
		imshow(windowName, deck[i].view);
	}*/

	//allow opencv window to stay open
	waitKey(0);
}


/*int main(int argc, char** argv)
{
	Mat image;
	image = imread("MY_IMAGE.jpg", IMREAD_COLOR); // Read the file
	if (image.empty()) // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}
	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Display window", image); // Show our image inside it.
	waitKey(0); // Wait for a keystroke in the window
	return 0;*
}*/